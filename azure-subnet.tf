resource "azurerm_subnet" "nucor_subnet" {
  for_each             = var.create_subnets ? var.nucor_subnets : {}
  name                 = each.value.name
  resource_group_name  = data.azurerm_resource_group.resource_group.name
  virtual_network_name = var.virtual_network_name
  address_prefixes     = each.value.address_prefixes

  dynamic "delegation" {
    for_each = each.value.delegation_block == null ? {} : each.value.delegation_block
    content {
      name = delegation.value.name

      dynamic "service_delegation" {
        for_each = delegation.value.service_delegation

        content {
          name    = service_delegation.value.name
          actions = service_delegation.value.actions
        }
      }
    }
  }

  depends_on = [azurerm_virtual_network.nucor_vnet]
}
