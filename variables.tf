variable "resource_group_name" {
  type = string
}

variable "subnet_name" {
  type = string
  default = null
}

variable "virtual_network_name" {
  type = string
}

variable "vnet_tags" {
  type = map(any)
  default = null
}

variable "subnet_tags" {
  type = map(any)
  default = null
}

variable "enforce_private_link_endpoint_network_policies" {
  type    = bool
  default = false
}

variable "enforce_private_link_service_network_policies" {
  type    = bool
  default = false
}

variable "create_virtual_network" {
  type    = bool
}

variable "create_subnets" {
  type    = bool
  default = false
}

variable "nucor_subnets" {
  type = map(object({
    name             = string
    address_prefixes = list(string)
    delegation_block = optional(map(object({
      name = string
      service_delegation = map(object({
        name    = string
        actions = optional(list(string))
      }))
    })))
  }))
  default = null
}

variable "service_endpoints" {
  type = list(string)
  default = null
}

variable "service_endpoint_policy_ids" {
  type = list(string)
  default = null
}

variable "vnet_dns_servers" {
  type = list(string)
  default = null
}

variable "vnet_address_space" {
  type = list(string)
  default = null
}

variable "vnet_bgp_community" {
  type = string
  default = null
}

variable "vnet_ddos_protection_plan" {
  type = map(object({
    id     = string
    enable = bool
  }))
  default = null
}