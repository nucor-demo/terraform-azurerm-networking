output "resource_group_name" {
  value = data.azurerm_resource_group.resource_group.name
}

output "resource_group_location" {
  value = data.azurerm_resource_group.resource_group.location
}

output "subnet_name" {
  value = values(azurerm_subnet.nucor_subnet).*.name
}

output "vnet_name" {
  value = var.create_virtual_network ? azurerm_virtual_network.nucor_vnet[0].name : var.virtual_network_name
}
